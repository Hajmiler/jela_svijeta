<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'title'=>$faker->name,
        'slug'=>$faker->name,
        'language_id'=>App\Language::all()->pluck('iso-label')->random(),
    ];
});
