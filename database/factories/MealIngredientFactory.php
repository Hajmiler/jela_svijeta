<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MealIngredient;
use App\Model;
use Faker\Generator as Faker;

$factory->define(MealIngredient::class, function (Faker $faker) {
    return [
        'meal_id'=>App\Meal::all()->pluck('id')->random(),
        'ingredient_id'=>App\Ingredient::all()->pluck('id')->random(),
    ];
});
