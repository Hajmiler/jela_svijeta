<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use App\Meal;
use Faker\Generator as Faker;

$factory->define(Meal::class, function (Faker $faker) {
    return [
        'title'=>$faker->name,
        'description'=>$faker->sentence($nbWords = 6, $variableNbWords = true),
        'status'=>'created',
        //'category_id'=>App\Category::all()->pluck('category_id'),
        'category_id'=>App\Category::all()->pluck('id')->random(),
    ];
});
