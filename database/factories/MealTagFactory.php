<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MealTag;
use Faker\Generator as Faker;

$factory->define(MealTag::class, function (Faker $faker) {
    return [
        'meal_id'=>App\Meal::all()->pluck('id')->random(),
        'tag_id'=>App\Tag::all()->pluck('id')->random(),
    ];
});
