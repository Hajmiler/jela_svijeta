<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredient_translations', function (Blueprint $table) {
             //mandatory fields
             $table->bigIncrements('id');
             $table->string('locale');
             //$table->timestamps();
 
             // Foreign key to the main model
             $table->unsignedBigInteger('ingredient_id');
             $table->unique(['ingredient_id', 'locale']);
             $table->foreign('ingredient_id')->references('id')->on('ingredients')->onDelete('cascade');
 
             // Actual fields you want to translate
             $table->string('title',100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredient_translations');
    }
}
