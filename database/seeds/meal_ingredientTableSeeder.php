<?php

use Illuminate\Database\Seeder;

class meal_ingredientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\MealIngredient::class, 2)->create();
    }
}
