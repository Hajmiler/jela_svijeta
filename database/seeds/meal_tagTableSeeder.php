<?php

use Illuminate\Database\Seeder;

class meal_tagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\MealTag::class, 2)->create();
    }
}
