<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = [
            ['title' => 'Hrvatski jezik', 'slug' => 'hrvatski-jezik', 'iso-label' => 'hr',],
            ['title' => 'Engleski jezik', 'slug' => 'engleski-jezik', 'iso-label' => 'en',],
            ['title' => 'Njemački jezik', 'slug' => 'njemacki-jezik', 'iso-label' => 'de',],

        ];

        foreach ($languages as $language) {
            App\Language::create($language);
        }
    }
}
