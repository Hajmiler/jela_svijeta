<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//package’s class used for translation
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Ingredient extends Model
{
    protected $table = 'ingredients';
    //protected $primaryKey = 'ingredient_id';

    use Translatable; //to add translation methods

    //define which attributes needs to be translated
    public $translatedAttributes = ['title'];

    //relationship between Meal and Ingredient model
    public function meals()
    {
        return $this->belongsToMany('App\Meal','meal_ingredient','meal_id','ingredient_id');
    }
}
