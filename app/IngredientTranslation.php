<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IngredientTranslation extends Model
{
    protected $fillable = ['title'];
    public $timestamps = false;

    //relationship between Language and CategoryTranslation model
    /*public function language()
    {
        return $this->belongsTo('App\Language','id');
    }*/
}
