<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MealTranslation extends Model
{
    protected $fillable = ['title', 'description'];
    public $timestamps = false;

    //relationship between Language and CategoryTranslation model
    /*public function language()
    {
        return $this->belongsTo('App\Language','id');
    }*/

}
