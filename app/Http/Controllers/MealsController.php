<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Meal;
use DB;


class MealsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$meals=DB::select('SELECT * FROM meals');
        //return response()->json($meals);
        //return view('index')->with('meals', $meals)->toJson(JSON_PRETTY_PRINT);

        //$meals = Meal::all()->toJson(JSON_PRETTY_PRINT);
        //return view('index')->with('meals', $meals);

        //$meals = Meal::with('category')->get()->toJson(JSON_PRETTY_PRINT);
        //return view('index')->with('meals', $meals);

        //rad na tagovima->radi
        $meals = Meal::with('category','tags','ingredients')->get()->toJson(JSON_PRETTY_PRINT);
        return view('index')->with('meals', $meals);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
