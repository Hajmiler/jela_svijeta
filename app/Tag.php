<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


//package’s class used for translation
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Tag extends Model
{
    protected $table = 'tags';
    //protected $primaryKey = 'tag_id';

    use Translatable; //to add translation methods

    //define which attributes needs to be translated
    public $translatedAttributes = ['title'];

    //relationship between Meal and Tag
    public function meals()
    {
        return $this->belongsToMany('App\Meal', 'meal_tag','meal_id','tag_id');
    }
    
}
