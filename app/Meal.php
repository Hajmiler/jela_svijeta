<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//package’s class used for translation
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Meal extends Model implements TranslatableContract
{
    protected $table = 'meals';
    //protected $primaryKey = 'meal_id';

    use Translatable; //to add translation methods

    //define which attributes needs to be translated
    public $translatedAttributes = ['title', 'description'];

    public function category()
    {
        return $this->belongsTo('App\Category','id');
    }

    //relationship between Meal and Tag
    public function tags()
    {
        return $this->belongsToMany('App\Tag','meal_tag','meal_id','tag_id');
    }

    //relationship between Meal and Ingredient model
    public function ingredients()
    {
        return $this->belongsToMany('App\Ingredient','meal_ingredient','meal_id','ingredient_id');
    }
}
