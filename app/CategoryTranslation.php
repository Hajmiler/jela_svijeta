<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    protected $table = 'category_translations';
    //protected $primaryKey = 'category_translation_id';
    //protected $foreignKey='category_id';

    protected $fillable = ['title'];
    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo('App\Category','id');
    }

    //relationship between Language and CategoryTranslation model
    /*public function language()
    {
        return $this->belongsTo('App\Language','id');
    }*/

}
