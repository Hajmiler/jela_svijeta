<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//package’s class used for translation
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Category extends Model implements TranslatableContract
{
    protected $table = 'categories';
    //protected $primaryKey = 'category_id';

    use Translatable; //to add translation methods

    //define which attributes needs to be translated
    public $translatedAttributes = ['title'];

    public function meals()
    {
        return $this->hasMany('App\Meal', 'category_id');
    }

    //relationship between Category and CategoryTranslation model
    public function categoryTranslations()
    {
        return $this->hasMany('App\CategoryTranslation','category_id');
    }
}
