<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MealIngredient extends Model
{
    protected $table = 'meal_ingredient';
    protected $foreign1='meal_id';
    protected $foreign2='ingredient_id';

    public $timestamps = false;
}
