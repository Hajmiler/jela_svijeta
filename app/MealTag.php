<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MealTag extends Model
{
    protected $table = 'meal_tag';
    protected $foreign1='meal_id';
    protected $foreign2='tag_id';

    public $timestamps = false;

}
